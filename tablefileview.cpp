#include "tablefileview.h"
#include <QStandardItemModel>

TableFileView::TableFileView(QWidget *parent) :
    QTableView(parent)
{
}

void TableFileView::updateModel()
{
    QStringList fileList = current_dir.entryList();
    int size = fileList.size();

    QAbstractItemModel* old_model = model();
    QStandardItemModel* model = new QStandardItemModel(size, 1, this);
    for (int i = 0; i < size; ++i)
    {
        QString item = fileList[i];
        QStandardItem *fileItem = new QStandardItem(QString(item));
        model->setItem(i, 0, fileItem);
    }
    setModel(model);
    delete old_model;
    selectRow(0);
}

void TableFileView::processUpKey()
{
    QModelIndexList indexList = selectionModel()->selection().indexes();
    int index = indexList.at(0).row();
    --index;
    if (index < 0)
        index = model()->rowCount() - 1;
    selectRow(index);
}

void TableFileView::processDownKey()
{
    QModelIndexList indexList = selectionModel()->selection().indexes();
    int index = indexList.at(0).row();
    ++index;
    if (index >= model()->rowCount())
        index = 0;
    selectRow(index);
}

void TableFileView::processEnterKey()
{
    QModelIndexList list = selectionModel()->selection().indexes();
    int index = list.at(0).row();
    QStandardItem* item = static_cast<QStandardItemModel*>(model())->item(index, 0);
    current_dir.cd(item->text());
    updateModel();
}
