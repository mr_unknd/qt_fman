#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QDir>
#include <QStandardItemModel>
#include <QKeyEvent>
#include <QDebug>



MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    QDir dir;
    ui->leftPanel->setCurrentDir(dir);
    ui->leftPanel->updateModel();
    ui->rightPanel->setCurrentDir(dir);
    ui->rightPanel->updateModel();

    ui->leftPanel->setSelectionMode(QAbstractItemView::SingleSelection);
    ui->leftPanel->setSelectionBehavior(QAbstractItemView::SelectRows);

    ui->rightPanel->setSelectionMode(QAbstractItemView::SingleSelection);
    ui->rightPanel->setSelectionBehavior(QAbstractItemView::SelectRows);

    ui->leftPanel->installEventFilter(this);
    ui->rightPanel->installEventFilter(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}

bool MainWindow::eventFilter(QObject *o, QEvent *event)
{
    if (event->type() == QEvent::KeyPress)
    {
        QKeyEvent* keyEvent = static_cast<QKeyEvent*>(event);
        if (keyEvent->key() == Qt::Key_Up)
        {
            static_cast<TableFileView*>(o)->processUpKey();
            qDebug() << keyEvent->key();
        }
        else if (keyEvent->key() == Qt::Key_Down)
        {
            static_cast<TableFileView*>(o)->processDownKey();
            qDebug() << keyEvent->key();
        }
        else if (keyEvent->key() == Qt::Key_Return)
        {
            static_cast<TableFileView*>(o)->processEnterKey();
        }
        return true;
    }
    else
    {
        // standard event processing
        return QObject::eventFilter(o, event);
    }
}

void MainWindow::on_tableView_clicked(const QModelIndex &index)
{

}

void MainWindow::on_pushButton_clicked()
{

}

void MainWindow::on_tableView_pressed(const QModelIndex &index)
{
}

