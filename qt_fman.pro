#-------------------------------------------------
#
# Project created by QtCreator 2015-03-24T15:37:46
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = qt_fman
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    tablefileview.cpp

HEADERS  += mainwindow.h \
    tablefileview.h

FORMS    += mainwindow.ui
