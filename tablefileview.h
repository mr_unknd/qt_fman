#ifndef TABLEFILEVIEW_H
#define TABLEFILEVIEW_H

#include <QTableView>
#include <QDir>

class TableFileView : public QTableView
{
    Q_OBJECT
public:
    explicit TableFileView(QWidget *parent = 0);

    void updateModel();

    void processUpKey();
    void processDownKey();
    void processEnterKey();

    const QDir& getCurrentDir() { return current_dir; }
    void setCurrentDir(QDir dir) { current_dir = dir; }

private:
    QDir current_dir;

signals:

public slots:

};

#endif // TABLEFILEVIEW_H
